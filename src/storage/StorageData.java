package storage;



public class StorageData {

    //private int numberOfLines;
    
    
    private double[] force;
    private double[] angle;
    private double[] Fx;
    private double[] Fy;
    private double windFx = 0;
    private double windFy = 0;
    private double sumFx = 0;
    private double sumFy = 0;
    private double higherValue = 0;
    private double scaleHigherFy = 0;
    private double forceWyp;
    private double angleWyp;
    private String[] line;
    private String[] profile;
    private String[] ownDestription;
    
    public StorageData(int numberOfLines){
        //this.numberOfLines = numberOfLines;
        force = new double[numberOfLines];
        angle = new double[numberOfLines];
        Fx = new double[numberOfLines];
        Fy = new double[numberOfLines];
        line = new String[numberOfLines];
        profile = new String[numberOfLines];
    }
    
    /*
    public void setNumberLines(int numberLines){
        this.numberLines = numberLines;
    }
    
    public int getNumberLines(){
        return numberLines;
    }*/
    
    
    public void setForce(double[] force){
        this.force = force;
    }
    
    public double[] getForce(){
        return force;
    }
    
    public double getForce(int i){
        return force[i];
    }
    
    public void setAngle(double[] angle){
        this.angle = angle;
    }
    
    public double[] getAngle(){
        return angle;
    }
    
        public double getAngle(int i){
        return angle[i];
    }
    
    public void setFx(double[] Fx){
        this.Fx = Fx;
    }
    
    public double[] getFx(){
        return Fx;
    }
    
    public double getFx(int i){
        return Fx[i];
    }
    
    public void setFy(double[] Fy){
        this.Fy = Fy;
    }
    
    public double[] getFy(){
        return Fy;
    }
    
    public double getFy(int i){
        return Fy[i];
    }
    
    public void setWindFx(double windFx){
        this.windFx = windFx;
    }
    
    public double getWindFx(){
        return windFx;
    }
    
    public void setWindFy(double windFy){
        this.windFy = windFy;
    }
    
    public double getWindFy(){
        return windFy;
    }
    
    public void setSumFx(double sumFx){
        this.sumFx = sumFx;
    }
    
    public double getSumFx(){
        return sumFx;
    }
    
    public void setSumFy(double sumFy){
        this.sumFy = sumFy;
    }
    
    public double getSumFy(){
        return sumFy;
    }
    
    public void setHigherValue(double higherValue){
        this.higherValue = higherValue;
    }
    
    public double getHigherValue(){
        return higherValue;
    }
    
    public void setScaleHigherFy(double scaleHigherFy){
        this.scaleHigherFy = scaleHigherFy;
    }
    
    public double getScaleHigherFy(){
        return scaleHigherFy;
    }
    
    public void setForceWyp(double forceWyp){
        this.forceWyp = forceWyp;
    }
    
    public double getForceWyp(){
        return forceWyp;
    }
    
    public void setAngleWyp(double angleWyp){
        this.angleWyp = angleWyp;
    }
    
    public double getAngleWyp(){
        return angleWyp;
    }
    
    public void setLine(String[] line){
        this.line = line;
    }
    
    public String[] getLine(){
        return line;
    }
    
    public String getLine(int i){
        return line[i];
    }
    
    public void setProfile(String[] profile){
        this.profile = profile;
    }
    
    public String[] getProfile(){
        return profile;
    }
    
    public String getProfile(int i){
        return profile[i];
    }
    
    public void setOwnDestription(String[] ownDestription){
        this.ownDestription = ownDestription;
    }
    
    public String[] getOwnDestription(){
        return ownDestription;
    }
    
    public String getOwnDestription(int i){
        return ownDestription[i];
    }
}
