package storage;


public class StorageProperties {

    private double acceptFw = 0;
    private double acceptFx = 0;
    private double acceptFy = 0;
    private boolean acceptFxFy = false;
    private boolean maxWindForce = false;
    private boolean icing = false;
    private double windForce = 0;
    private double windAngle = 0;
    private boolean objectStrong = false;
    private boolean simpleDescription = false;
    private String poleName = null;
    private String units = null;
    
    
    
    
    public void setAcceptFw(double acceptFw){
       this.acceptFw = acceptFw;
   }
    
    public double getAcceptFw(){
       return acceptFw;
   }
    
    public void setAcceptFx(double acceptFx){
       this.acceptFx = acceptFx;
   }
   
   public double getAcceptFx(){
       return acceptFx;
   }
   
   public void setAcceptFy(double acceptFy){
       this.acceptFy = acceptFy;
   }
   
   public double getAcceptFy(){
       return acceptFy;
   }
   
   public void setAcceptFxFy(boolean acceptFxFy){
       this.acceptFxFy = acceptFxFy;
   }
   
   public boolean isAcceptFxFy(){
       return acceptFxFy;
   }
   
   public void setMaxWindForce(boolean maxWindForce){
       this.maxWindForce = maxWindForce;
   }
   
   public boolean isMaxWindForce(){
       return maxWindForce;
   }
   
   public void setIcing(boolean icing){
       this.icing = icing;
   }
   
   public boolean isIcing(){
       return icing;
   }
   
   public void setWindForce(double windForce){
       this.windForce = windForce;
   }

  public double getWindForce(){
       return windForce;
   }
  
  public void setWindAngle(double windAngle){
       this.windAngle = windAngle;
   }
  
  public double getWindAngle(){
       return windAngle;
   }
  
  public void setObjectStrong(boolean objectStrong){
       this.objectStrong = objectStrong;
   }
  
  public boolean isObjectStrong(){
        return objectStrong;
    }
   
  public void setSimpleDescription(boolean ownDescription){
       this.simpleDescription = ownDescription;
   }
  
  public boolean isSimpleDescription(){
        return simpleDescription;
    }
    
   public void setPoleName(String poleName){
       this.poleName = poleName;
   }
  
  public String getPoleName(){
        return poleName;
    }
  
  public void setUnits(String units){
       this.units = units;
   }
  
  public String getUnits(){
        return units;
    }
}
