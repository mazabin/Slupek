package slupek;

import java.io.File;
import javax.swing.filechooser.FileFilter;


  public class ExtensionFileFilter extends FileFilter {
      
  String description;
  String extensions[];

  
  public ExtensionFileFilter(String description, String extension) {
    this(description, new String[] { extension });
  }

  public ExtensionFileFilter(String description, String extensions[]) {

    this.description = description;
    this.extensions =  extensions;
   // toLower(this.extensions);
  }

  /*private void toLower(String array[]) {
    for (int i = 0, n = array.length; i < n; i++) {
      array[i] = array[i].toLowerCase();
    }*/
  

  @Override
  public String getDescription() {
    return description;
  }

  // ignore case, always accept directories
  // character before extension must be a period
  @Override
  public boolean accept(File file) {
      //System.out.println(extensions);
     if (file.isDirectory()) {
        return true;
      }
      int count = extensions.length;
      String path = file.getAbsolutePath();
      for (int i = 0; i < count; i++) {
        String ext = extensions[i];
        if (path.endsWith(ext) && (path.charAt(path.length() - ext.length()) == '.')) {
          return true;
        }
      }
      return false;
  }
}  
    
    

