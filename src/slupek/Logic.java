package slupek;

import storage.StorageProperties;
import storage.StorageData;
import static java.lang.Math.abs;
import static java.lang.Math.atan;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import java.util.stream.DoubleStream;



public class Logic {
    
    int numberOfLines;
    double[] force;
    double[] angle;
    double forceWyp;
    double angleWyp;
    double[] Fx;
    double[] Fy;
    
    double windForce;
    double windFx;
    double windFy;
    double windAngle;
    
    //boolean maxWindForce = false;
    //boolean objectStrong;
    
    boolean acceptFxFy = false;
    double acceptFw = 0;
    double acceptFx = 0;
    double acceptFy = 0;
   
    double sumFx;
    double sumFy;
    double higherValue = 0; 

    private StorageData data;
    private StorageProperties dataProp;
    
    Logic(int numberOfLines){
        this.numberOfLines = numberOfLines;
        
        force = new double[numberOfLines];
        angle = new double[numberOfLines];
        Fx = new double[numberOfLines];
        Fy = new double[numberOfLines];
        
        for(int i=0 ; i<numberOfLines ; i++){
            force[i] = 0;
            angle[i] = 0;
            Fx[i] = 0;//????????
            Fy[i] = 0;//???
        }
    }
    
    
    public void forces(){
        if(data != null){// kopia wartosci do savedata
        force = data.getForce();
        angle = data.getAngle();
        }
        //System.out.println(" skkkky "+angleField[0]);
       for(int i=0 ; i<numberOfLines ; i++){
          Fx[i] = round(force[i]*cos(Math.toRadians(angle[i])),6);
          Fy[i] = round(force[i]*sin(Math.toRadians(angle[i])),6);
          //System.out.println(Fx[i]+" siłyyyyyy "+Fy[i]);
       }
       if(data != null){// kopia wartosci do savedata
        data.setFx(Fx);
        data.setFy(Fy);
        windForce = dataProp.getWindForce();// dane do obl siły wiatru niżej
        windAngle = dataProp.getWindAngle();
        }
       
       
       //dodanie siły wiatru
       
       if(dataProp!=null&&dataProp.isMaxWindForce()){
           sumFx = DoubleStream.of(Fx).sum();
            sumFy = DoubleStream.of(Fy).sum();
            angleWyp = Math.toDegrees(atan(sumFy/sumFx));
           convertAngle();// ustawienie kąta żeby nie był na minusie
          windFx = round(windForce*cos(Math.toRadians(angleWyp)),6);
          windFy = round(windForce*sin(Math.toRadians(angleWyp)),6); 
       }else if(dataProp!=null){
            windFx = round(windForce*cos(Math.toRadians(windAngle)),6);
            windFy = round(windForce*sin(Math.toRadians(windAngle)),6); 
       }
       
            sumFx = DoubleStream.of(Fx).sum()+windFx;
            sumFy = DoubleStream.of(Fy).sum()+windFy;
            angleWyp = Math.toDegrees(atan(sumFy/sumFx));       
       forceWyp = sumFx/(cos(atan(sumFy/sumFx)));
       
       angleWyp = round(angleWyp,2); //zaokrąglanie
       forceWyp = abs(round(forceWyp,2));       
       
       
       convertAngle();
       higherValue();
       checkFmax();
       //System.out.println(F[0]+"  "+alfa[0]);
       //System.out.println(sumFx+" siły "+sumFy);
       //System.out.println((cos(angleWyp*PI/180))+"  "+Fy[0]);
       //System.out.println(forceWypField+"  "+angleWyp);
       //System.out.println(higherValue+"scall  ");
       
       
       if(data != null){// kopia wartosci do savedata
        data.setWindFx(windFx);
        data.setWindFy(windFy);
        data.setSumFx(sumFx);
        data.setSumFy(sumFy);
        data.setForceWyp(forceWyp);
        data.setAngleWyp(angleWyp);
        }
    }
    
    private void convertAngle(){
        
        if(sumFx<0 && sumFy>0){
          angleWyp+=180; 
       }else if(sumFx<0 && sumFy<0){
           angleWyp+=180; 
       }else if(sumFx>0 && sumFy<0){
           angleWyp+=360; 
       }
        
    }
    
    
    void checkFmax(){
        //System.out.println(acceptFxFy+"acceptFxFy"+sumFx+"sumFx"+acceptFx+"acceptFx");
        if(dataProp!=null){
        acceptFxFy = dataProp.isAcceptFxFy();
        
        if(acceptFxFy&&sumFx<=dataProp.getAcceptFx()&&sumFy<=dataProp.getAcceptFy()){
            //System.out.println(acceptFxFy+"acceptFxFy"+sumFx+"sumFx"+acceptFx+"acceptFx");
            dataProp.setObjectStrong(true);
        }else {
            dataProp.setObjectStrong( !acceptFxFy&&forceWyp<=dataProp.getAcceptFw());
            //System.out.println(acceptFxFy+"acceptFxFy"+forceWyp+"forceWyp"+acceptFw+"acceptFw");
        }
        }
    }
    
    
    double round(double roundValue, int precision ){ // zaokrąglanie w celu uniknięcia szumów z obliczeń
        roundValue *= 10 * precision;
        roundValue = Math.round(roundValue);
        roundValue /= 10* precision; 
        return roundValue;
    }
  
    
    
    void higherValue(){
        higherValue = 0;
  
  for(int i=0 ; i<force.length ; i++){
     if(abs(Fx[i]) > higherValue)
      higherValue = abs(Fx[i]);
     if(abs(Fy[i]) > higherValue)
      higherValue = abs(Fy[i]);
  }
  
  if(abs(sumFx) > higherValue)//wartość bezwzględna
      higherValue = abs(sumFx);
  if(abs(sumFy) > higherValue)//wartość bezwzględna
      higherValue = abs(sumFy);
  
  if(abs(windFx) > higherValue)//wartość bezwzględna
      higherValue = abs(windFx);
  if(abs(windFy) > higherValue)//wartość bezwzględna
      higherValue = abs(windFy);
  
  if(data != null){// kopia wartosci do savedata
        data.setHigherValue(higherValue);
        }
  
  
  //System.out.println(windFy+" windFx "+windFx);
  //System.out.println(higherValue+" higherValue "+higherValue);
    }
    
    
     public void  setStorageData(StorageData data){
        this.data = data;
    }
     
     public void  setStorageProperties(StorageProperties dataProp){
        this.dataProp = dataProp;
    }
    /*void setWestPanel(UserSpace westPanel){
        this.westPanel = westPanel;
    }*/
    /*void setForceGraph(ForceGraph forceGraph){
        this.forceGraph = forceGraph;
    }*/
/*
    public void setMaxWindForce(boolean maxWindForce){
        this.maxWindForce = maxWindForce;
    }
    
    public void setWindForce(double windForce){
        this.windForce = windForce;
    }
    
    public void setWindAngle(double windAngle){
        this.windAngle = windAngle;
    }
    
    public void setAcceptFxFy(boolean acceptFxFy){
        this.acceptFxFy = acceptFxFy;
    }
    
    public void setAcceptFw(double acceptFw){
        this.acceptFw = acceptFw;
    }
    
    public void setAcceptFx(double acceptFx){
        this.acceptFx = acceptFx;
    }
    
    public void setAcceptFy(double acceptFy){
        this.acceptFy = acceptFy;
    }*/
    /*
    public double getForceWyp(){
        return abs(round(forceWyp,2));
    }
    
    public double getAlfaWyp(){
        return round(angleWyp,2);
    }*/
    
    /*public double getHigherValue(){
        return higherValue;
    }*/
    
   /* public double[] getFx(){
        return Fx;
    }
    
    public double[] getFy(){
        return Fy;
    }*/
    
    /*public double getWindFx(){////////////////////////////////////
        return windFx;
    }
    
    public double getWindFy(){
        return windFy;
    }*/
    /*public double getSumFx(){
        return sumFx;
    }
    
    public double getSumFy(){
        return sumFy;
    }*/
    
    /*public boolean isObjectStrong(){
        return objectStrong;
    }*/
    
    
    /*
    public double[] getScaleFx(){
        return scaleFx;
    }
    
    public double[] getScaleFy(){
        return scaleFy;
    }
    
    public double getScaleFxWyp(){
        return scaleFxWyp;
    }
    
    public double getScaleFyWyp(){
        return scaleFyWyp;
    }
    */
}
