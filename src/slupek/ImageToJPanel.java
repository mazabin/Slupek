package slupek;


import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import java.awt.image.BufferedImage;

import javax.swing.JPanel;

// można pokombinować i rozszerzyć działanie klasy np na sklejanie obrazów lewo prawo albo nakładanie na siebie

public class ImageToJPanel extends JPanel {
   
   //!!!!components like JPanel, JComboBox, JScrollPane need to be layed out before then can be painted
   BufferedImage buffImageUp;
   BufferedImage buffImageDown;
   
    ImageToJPanel(BufferedImage buffImageUp){
        this.buffImageUp = buffImageUp;
    }
    
    ImageToJPanel(BufferedImage buffImageUp , BufferedImage buffImageDown){
        this.buffImageUp = buffImageUp;
        this.buffImageDown = buffImageDown;  
    }

    @Override
   protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D)g;
    /*
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);//co to jest i do czego
    ////////
    g2.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    g2.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
    g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
*/
    ///////
    
    g2.drawImage(buffImageUp,0,0,this);
    if(buffImageDown!=null)
    g2.drawImage(buffImageDown,0,buffImageUp.getHeight(),this);
    //g2.drawLine(getWidth()/2, BORDER_GAP, getWidth()/2,  getHeight() - BORDER_GAP);
     
     
   }
   
  @Override 
   public Dimension getPreferredSize() { 	
      return new Dimension(buffImageUp.getWidth(),buffImageUp.getHeight());
   } 

}


