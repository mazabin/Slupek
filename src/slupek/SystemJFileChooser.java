package slupek;

import java.io.File;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;//szybkie wiadomości typu wpisz coś, wiadomość jakaś, prosty wybór 
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class SystemJFileChooser extends JFileChooser{
    
   
    
   
    
    LookAndFeel previousLF = UIManager.getLookAndFeel();
    
    
   @Override
   public void updateUI(){// wstawia wygląd Windowsowy (gdy pracujemy na windowsie)
      try {
         UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
         
      } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
           Logger.getLogger(SystemJFileChooser.class.getName()).log(Level.SEVERE, null, ex);
       }
      
      super.updateUI();
   }
   
   
  
    @Override
    public void approveSelection(){// wywołuje okienko podczas akcji save potwierdzajace czy zamienić plik
        File f = getSelectedFile();
        
        if(f.exists() && getDialogType() == SAVE_DIALOG){
            int result = JOptionPane.showConfirmDialog(this,"The file exists, overwrite?","Existing file",JOptionPane.YES_NO_CANCEL_OPTION);
            switch(result){
                case JOptionPane.YES_OPTION:
                    super.approveSelection();
                    return;
                case JOptionPane.NO_OPTION:
                    return;
                case JOptionPane.CLOSED_OPTION:
                    return;
                case JOptionPane.CANCEL_OPTION:
                    cancelSelection();
                    return;
            }
        }
        super.approveSelection();
    }        
}
