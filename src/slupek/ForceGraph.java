package slupek;

import storage.StorageProperties;
import storage.StorageData;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import static java.lang.Math.abs;
import static java.lang.Math.atan;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import javax.swing.JPanel;


public class ForceGraph extends JPanel {
    
    private static final int PREF_W = 700;
    private static final int PREF_H = 700;
    private static final int BORDER_GAP = 40;
    private static final int AXSIS_GAP = 20;
    private static final Color WYPFORCE_COLOR = Color.red;
    private static final Stroke VECTOR_STROKE = new BasicStroke(3f);
    private static final int GRAPH_POINT_WIDTH = 7;
    private static final int HATCH_CNT = 10;
    private static final Stroke BASIC_STROKE = new BasicStroke();
    private static final String FONT_NAME = "Serif";
    private static final int FONT_STYLE = java.awt.Font.ROMAN_BASELINE;
    private static final int FONT_SIZE = 10;
    private static final int SCALE = 1400;
    private Font fontType;
    private FontMetrics metrics;
 
    private int numberOfLines;
    private double[] scaleFx;
    private double[] scaleFy;
    //private double scaleWindFx;
   // private double scaleWindFy;
    //private double scaleFxWyp;
    //private double scaleFyWyp;
    private double scAxeWidth;
    private double scAxeHeight;
    private int scFont;
    ///////////////////////////////////////
    private Rectangle2D[] nameRec;
    private boolean[] overlaps;
    //private Rectangle2D windRec;
    //private Rectangle2D forceWypRec;
    private int[] quarter;
    private Point2D[] pointVector;
    private String[] vectorName;
    private Point2D[] pointName;
    //private String[] vectorChangeName;
    ////////////////////////////////
    private AffineTransform basic;

    private Control control;
    private StorageData data;
    private StorageProperties dataProp;

    ForceGraph(int numberOfLines) {
        this.numberOfLines = numberOfLines;
        scaleFx = new double[numberOfLines+2];
        scaleFy = new double[numberOfLines+2];
        nameRec = new Rectangle2D[numberOfLines+2];//!!!!!!!!!!!!!!!!!
        overlaps = new boolean[numberOfLines+2]; //!!!!!!!!!!!!!!!!!!!!
        quarter = new int[numberOfLines+2];//!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        pointVector = new Point2D[numberOfLines+2];//!!!!!!!!!!!!!!!!!!!!
        vectorName = new String[numberOfLines+2];//!!!!!!!!!!!!!!!!
        pointName = new Point2D[numberOfLines+2];//!!!!!!!!!!!!!!!!
        //vectorChangeName = new String[numberOfLines+2];//!!!!!!!!!!!!!!!!
        setBackground(Color.WHITE);
        setLayout(null);

        resetVectorName();
        
    }
    
    private void resetVectorName(){
        for (int i = 0; i < numberOfLines+2; i++) {
         overlaps[i] = true;
         nameRec[i]=null;
         vectorName[i] = "F" + (i + 1);
         if(i==numberOfLines){
             vectorName[i] = "Fwin";
         }else if(i==numberOfLines+1){
             vectorName[i] = "Fw";
         }
        }
        
    }


   
   
    @Override
   protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D)g;
    
    resetVectorName();
    basic = g2.getTransform(); 
    scAxeWidth = (double)getWidth()/(double)PREF_W; 
    scAxeHeight = (double)getHeight()/(double)PREF_H;
    scFont = (getHeight()+ getWidth())/SCALE;
    if(scFont<1)
        scFont = 1;
    
    if(control!=null){
       control.updateGraph();
    }

    fontType = new Font(FONT_NAME, FONT_STYLE, FONT_SIZE*scFont);
    metrics = g2.getFontMetrics(fontType);// get metrics from the graphics
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);//co to jest i do czego
      
    
    g2.setStroke(BASIC_STROKE);
    g2.setFont(fontType);  
    
    /////////////// create x and y axes 
    //create y axe

    g2.draw(new Line2D.Double(getWidth()/2, BORDER_GAP*scAxeHeight, getWidth()/2, getHeight() - BORDER_GAP*scAxeHeight));
    // create y arrow axe
    g2.setTransform(setAT(getWidth()/2, BORDER_GAP*scAxeHeight, 90));
    g2.draw(new Arrow(getHeight()/2 - (BORDER_GAP + AXSIS_GAP)*scAxeHeight, 0));//poprawic
    g2.setTransform(basic);
    
    //create x axe
    g2.draw(new Line2D.Double(BORDER_GAP*scAxeWidth,getHeight()/2,getWidth() - BORDER_GAP*scAxeWidth,getHeight()/2));
    // create x arrow axe
    g2.setTransform(setAT(getWidth() - BORDER_GAP*scAxeWidth, getHeight()/2, 0));      
    g2.draw(new Arrow(getWidth()/2 - (BORDER_GAP + AXSIS_GAP)*scAxeWidth, 0));//poprawic  
    g2.setTransform(basic);       

    
    
    //System.out.println(getWidth()+"  :scAxeWidth  w" + scAxeWidth);
      // create hatch marks for y axis. 
      for (int i = 0; i <= HATCH_CNT; i++) {

         double x0 = getWidth()/2 - GRAPH_POINT_WIDTH*scAxeHeight;
         double x1 = getWidth()/2 + GRAPH_POINT_WIDTH*scAxeHeight;
         double y0 = getHeight() - (i*(getHeight() - 2*(BORDER_GAP + AXSIS_GAP)*scAxeHeight) /HATCH_CNT
                 + (BORDER_GAP + AXSIS_GAP)*scAxeHeight);
         double y1 = y0;
         g2.draw(new Line2D.Double(x0, y0, x1, y1));
      }

      // and for x axis
      for (int i = 0; i <= HATCH_CNT; i++) {
         
         double x0 = getWidth() - (i*(getWidth() - 2*(BORDER_GAP + AXSIS_GAP)*scAxeWidth) /HATCH_CNT
                 + (BORDER_GAP + AXSIS_GAP)*scAxeWidth);
         double x1 = x0;
         double y0 = getHeight()/2 - GRAPH_POINT_WIDTH*scAxeWidth;
         double y1 = getHeight()/2 + GRAPH_POINT_WIDTH*scAxeWidth;
         g2.draw(new Line2D.Double(x0, y0, x1, y1));
      }
      
      // create name for y axsis
      //int quar1 = checkQuarter(getWidth()/2,getHeight()/2,getWidth()/2+HATCH_CNT,BORDER_GAP*scAxeHeight);
    //Point2D pointNameAxsisY = drawVectorName(quar1,getWidth()/2+HATCH_CNT,BORDER_GAP*scAxeHeight);
    g2.drawString("Y", getWidth()/2+(int)(BORDER_GAP/2*scAxeWidth),(int)(BORDER_GAP*scAxeHeight)); 
    
    // create name for x axsis
    //int quar2 = checkQuarter(getWidth()/2,getHeight()/2,getWidth() - BORDER_GAP*scAxeWidth,getHeight()/2+HATCH_CNT);
    //Point2D pointNameAxsisX = drawVectorName(quar2,getWidth() - BORDER_GAP*scAxeWidth,getHeight()/2+HATCH_CNT);
    g2.drawString("X", getWidth() - (int)(BORDER_GAP*scAxeWidth),getHeight()/2-(int)(BORDER_GAP/2*scAxeHeight)); 

      
    // rysowanie sił+siły wiatru(i = numberOfLines)+siły wyp(i=numberOfLines+1)
        for (int i = 0; i < numberOfLines+2; i++) {
            g2.setStroke(VECTOR_STROKE);

            if (scaleFx[i] != 0 || scaleFy[i] != 0) {
                if (i == numberOfLines + 1) {
                    g2.setColor(WYPFORCE_COLOR);
                    drawVector(i, g2, scaleFx[i], scaleFy[i]);
                    g2.setColor(Color.BLACK);
                } else {
                    drawVector(i, g2, scaleFx[i], scaleFy[i]);
                }

            }
        }

      // sprawdzanie nakładania się nazw // rysowanie granic prostokątnych wokół nazw
     for (int i = 0; i < numberOfLines+2; i++) {
         /*String s = "F" + (i + 1);
         if(i==numberOfLines){
             s = "Fwin";
         }else if(i==numberOfLines){
             s = "Fm";
         }*/
         
         if (scaleFx[i] != 0 || scaleFy[i] != 0) { 
            // System.out.println((scaleFx[i] != 0 || scaleFy[i] != 0)+"nameRec[i]"+i+"scaleFx"+scaleFx[i]+"scaleFy"+scaleFy[i]);
           makeRec(i); 
         } 
     }
        

        
        // podpisywanie sił+siły wiatru(i = numberOfLines)+siły wyp(i=numberOfLines+1)
            g2.setStroke(VECTOR_STROKE);
        for (int i = 0; i < numberOfLines+2; i++) {
            /*String s = "F" + (i + 1);
            if(i==numberOfLines){
             s = "Fwin";
         }else if(i==numberOfLines){
             s = "Fm";
         }*/
            
            if (scaleFx[i] != 0 || scaleFy[i] != 0) {
                 //System.out.println((scaleFx[i] != 0 || scaleFy[i] != 0)+"drawNameVector[i]"+i);
                checkNameVector(i);
            }
        }
        
        for (int i = 0; i < numberOfLines+2; i++) {
            if(nameRec[i]!= null){
          //g2.draw(nameRec[i]);  // rysuje prostokąty wokół nazw wektorów - wyłączony
          //drawNameVector(i,g2);
          g2.drawString(vectorName[i], (int)pointName[i].getX(), (int)pointName[i].getY()); 
          
            }
        }
      
    
   }
   
   private void drawVector(int j,Graphics2D g2d, double Fx, double Fy){
      //można połączyć strzałkę z linią żeby teg mniej było 
    double x0 = getWidth()/2;
    double x1 = getWidth()/2 + Fx;
    double y0 = getHeight()/2;
    double y1 = getHeight()- (getHeight()/2+ Fy);
    double angle = Math.toDegrees(atan(Fy/Fx));
    double vectorLenght;
   
    g2d.draw(new Line2D.Double(x0, y0, x1, y1));

    if(abs(Fx)>=abs(Fy)){//ustawianie rozmiaru i położenia strzałek
     vectorLenght =Fx/cos(Math.toRadians(angle));
    }else{
     vectorLenght =Fy/sin(Math.toRadians(angle));  
    }
    
    g2d.setTransform(setAT(x1,y1,angle));// dynamiczna rotacja strzalki
    g2d.draw(new Arrow(vectorLenght,0));
    g2d.setTransform(basic);
    
    quarter[j] = checkQuarter(x0,y0,x1,y1);
    pointVector[j] = new Point2D.Double(x1,y1);
   }
   
    private void checkNameVector(int i) {

        for (int j = 0; j < nameRec.length; j++) {
            if ((scaleFx[j] != 0 || scaleFy[j] != 0) && (nameRec[i] != null && nameRec[j] != null) //sprawdzamy czy 
                    && j != i && overlaps[j] && overlaps(nameRec[i], nameRec[j])) {//cz nakładają się
// sprawdzamy przy ktorym vektorze wpisac nazwę

                if (checkPosition(j, pointVector[i], pointVector[j])) {//true daje nam pointVector[i]
                    vectorName[i] = vectorName[i] + ", " + vectorName[j]; // tablica zmienionych nazw
                    vectorName[j] = null;
                    overlaps[j] = false;//wyłączamy z obliczeń drugi wektor nachodzący
                    makeRec(i); // od nowa trzeba naysowac granice z nowym vectorName
                    nameRec[j] = null;
                } else {
                    vectorName[j] = vectorName[i] + ", " + vectorName[j];
                    vectorName[i] = null;
                    overlaps[i] = false;//wyłączamy z obliczeń drugi wektor nachodzący 
                    makeRec(j); // od nowa trzeba naysowac granice z nowym vectorName
                    nameRec[i] = null;
                }

            }

        }

    }
   
   
   
   private boolean checkPosition(int j, Point2D p1, Point2D p2){//czy p1 ma byc pozycją wyjściową
     boolean p0 = false;//true - p1
     
     if(quarter[j]==1 || quarter[j]==2){
         p0 = p1.getY()<p2.getY(); 
     
     }else if(quarter[j]==3 || quarter[j]==4){
         p0 = p1.getY()>p2.getY();
     
     }
       return p0;
   }
   
   
    private int checkQuarter(double x0, double y0, double x1, double y1) {

        int quar = 0;

        if (x1 >= x0 && y1 <= y0) {//pierwsza ćwiartka
            quar = 1;
        } else if (x1 <= x0 && y1 <= y0) {//druga
            quar = 2;
        } else if (x1 < x0 && y1 > y0) {//trzecia
            quar = 3;
        } else if (x1 >= x0 && y1 >= y0) {//czwarta
            quar = 4;
        }
        return quar;
    }

   /*private Point2D.Double drawVectorName(int i,Rectangle2D rec, double y1){// można dać gdzieś indziej 
    double width = rec.getCenterX();
       if (i == 1) {//pierwsza ćwiartka
           y1 -= FONT_SIZE * scFont / 4;
       } else if (i == 2) {//druga
           y1 -= FONT_SIZE * scFont / 4;
           x1 -= FONT_SIZE * scFont;
       } else if (i == 3) {//trzecia
           y1 += 5 * FONT_SIZE * scFont / 4;
           x1 -= FONT_SIZE * scFont;
       } else if (i == 4) {//czwarta
           y1 += 5 * FONT_SIZE * scFont / 4;
       }
    
    return new Point2D.Double(x1,y1);
}*/
  

private void makeRec(int i){// można dać gdzieś indziej 
    
    double x1 = pointVector[i].getX();
    double y1 = pointVector[i].getY();
// get the height of a line of text in this font and render context
    int hgt = metrics.getHeight();
// get the advance of my text in this font and render context
    int adv = metrics.stringWidth(vectorName[i]);

    if(quarter[i]==1 || quarter[i]==2){
nameRec[i] =  new Rectangle2D.Double(x1-adv/2,y1-hgt,adv,hgt);
pointName[i] = new Point2D.Double(x1-adv/2,y1-hgt/4);
     }else if(quarter[i]==3 || quarter[i]==4){
nameRec[i] =  new Rectangle2D.Double(x1-adv/2,y1,adv,hgt);
pointName[i] = new Point2D.Double(x1-adv/2,y1+3*hgt/4);
     }
    
    //return new Rectangle2D.Double(x1-adv/2,y1-4*hgt/4,adv,hgt);
}
   

   
   
    @Override
   public Dimension getPreferredSize() {
      return new Dimension(PREF_W, PREF_H);
   }
    
   
   
    private AffineTransform setAT(double x1, double y1, double theta) {
        AffineTransform at = new AffineTransform();
        at.setTransform(basic);
        at.translate(x1, y1);
//at.translate(x, y); ustawiamy położenie początkowe figury       
//at.rotate(kąt, położenie punktu wokół którego obraca sie figura x, położenie punktu wokół którego obraca sie figura y);
//położenie domyślne początkowe figury       
// at.rotate(x,y,getWidth()/2, getHeight()/2);
        at.rotate(Math.toRadians(-theta));

        return at;
    }
 
 void scaleVectors(){
     if(data!=null){// może niepotrzebny
    for (int i = 0; i < numberOfLines; i++) {
        
       scaleFx[i] = scaleVectorWidth(data.getFx(i)); 
       scaleFy[i] = scaleVectorHeight(data.getFy(i)); 
    }
    //scaleWindFx
      scaleFx[numberOfLines] = scaleVectorWidth(data.getWindFx());
    //scaleWindFy 
      scaleFy[numberOfLines] = scaleVectorHeight(data.getWindFy()); 
      
    //scaleFxWyp  
      scaleFx[numberOfLines+1] = scaleVectorWidth(data.getSumFx());
    //scaleFyWyp
      scaleFy[numberOfLines+1] = scaleVectorHeight(data.getSumFy()); 
      
      scaleHigherFy();
     }
     
}
 
    private void scaleHigherFy() {//dynamiczne obcinanie wykresu

        double scaleHigherFy = (BORDER_GAP * scAxeHeight + getHeight() / 2) / getHeight();// nic nie wpiszemy i obetnie dolną część wykresu
        double counter = 0;

        
        for (int i = 0; i < numberOfLines + 2; i++) {

            if (scaleFy[i] < counter) {
                counter = scaleFy[i];
                double change = ((getHeight() / 2) - (BORDER_GAP +AXSIS_GAP) * scAxeHeight) / 5;//zaoktągla wartosć do jednosci w dół -1.09-->-2
                int roundValue = (int) Math.round(scaleFy[i] / change - 0.5);// po odjęciu zaokrągla wartość change
                if(roundValue<-5){//poprawić!!!!!!!111
                    roundValue=-5;
                }
                double finalValue = change*roundValue;
                //System.out.println(roundValue+"   finalValue  " + finalValue);
                scaleHigherFy = (BORDER_GAP * scAxeHeight + getHeight() / 2 - finalValue) / getHeight();
            }

        }
        data.setScaleHigherFy(scaleHigherFy);

    }
 
 double scaleVectorWidth(double fx){      
    return scaleValue(data.getHigherValue(), fx, getWidth() / 2 - BORDER_GAP*scAxeWidth);    
}
 
 double scaleVectorHeight(double fy){
    return scaleValue(data.getHigherValue(), fy, getHeight() / 2 - BORDER_GAP * scAxeHeight);
} 
  
double scaleValue(double maxValue, double Value, double AxsisScale) {
        if (maxValue == 0) {
            return 0;
        } else {
            return Value * AxsisScale / maxValue;
        }
    }


 public boolean overlaps (Rectangle2D r1,Rectangle2D r2) {// sprawdza czy nakladają się na siebie prostokąty(nazwy vektorów)
      // Left x 
    double leftX = Math.max(r1.getMinX(), r2.getMinX());
    // Right x
    double rightX = Math.min(r1.getMaxX(), r2.getMaxX());
    // Bottom y
    double botY = Math.max(r1.getMinY(), r2.getMinY());
    // TopY
    double topY = Math.min(r1.getMaxY(), r2.getMaxY());

    return rightX > leftX && topY > botY;
} 





void setControl(Control control){
       this.control = control;
   } 

 public void  setStorageData(StorageData data){
        this.data = data;
    }
 
 public void  setStorageProperties(StorageProperties dataProp){
        this.dataProp = dataProp;
    }
 
}