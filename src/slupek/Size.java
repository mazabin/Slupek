package slupek;

import java.awt.Dimension;


public  enum Size{
       //A3(),  //297x420mm , 300dpi
       A4(2480 , 3508),  //210x297mm , 300dpi
       //A6(),  //105x148mm , 300dpi
       A8(620 , 877),  //52x74mm , 300dpi
       A700(495,700);
       
       private final int width , height;
       
       private Dimension dim;
       
       private Size(int width, int height){
           this.width = width;
           this.height = height;
           //dim.setSize(width, height);
       }
       
       public int getWidth(){
        return width;
       }
       
       public int getHeight(){
        return height;
       }  
       /*
        public Dimension getDimension(){
        return dim;
       } */
   }