package slupek;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;


public class IntFilter extends DocumentFilter{
    
    
    
    //private Pattern regexCheck = Pattern.compile("[0-9]+");
    private int maxCharacters = 10; 
   @Override
   public void insertString(DocumentFilter.FilterBypass fb, int offset, String string,
         AttributeSet attr) throws BadLocationException {

      Document doc = fb.getDocument();
      StringBuilder sb = new StringBuilder();
      sb.append(doc.getText(0, doc.getLength()));
      sb.insert(offset, string);

      if (sb.toString().matches("^[0-9]+[.]?[0-9]{0,}$")&& doc.getLength()+string.length() <= maxCharacters) {//  ""^\\d+\\.?\\d*$"// clss pattern
         super.insertString(fb, offset, string, attr);
         //System.out.println(sb.toString());
         
      } else {
         // warn the user and don't allow the insert // Toolkit.getDefaultToolkit().beep(); 
      }
   }

   @Override
   public void replace(DocumentFilter.FilterBypass fb, int offset, int length, String text,
         AttributeSet attrs) throws BadLocationException {

      Document doc = fb.getDocument();
      StringBuilder sb = new StringBuilder();
      sb.append(doc.getText(0, doc.getLength()));
      sb.replace(offset, offset + length, text);

      if (sb.toString().matches("^[0-9]+[.]?[0-9]{0,}$")&& doc.getLength()+text.length()-length <= maxCharacters) {
         super.replace(fb, offset, length, text, attrs);
         //System.out.println(sb.toString());
         
      } else {
         // warn the user and don't allow the insert
      }

   }

   @Override
   public void remove(DocumentFilter.FilterBypass fb, int offset, int length)
         throws BadLocationException {
      Document doc = fb.getDocument();
      StringBuilder sb = new StringBuilder();
      sb.append(doc.getText(0, doc.getLength()));
      sb.delete(offset, offset /*+ length*/);

      if (sb.toString().matches("^[0-9]+[.]?[0-9]{0,}$")&& doc.getLength()-length <= maxCharacters) {
         super.remove(fb, offset, length);
         
         //System.out.println(sb);
      } else {
         // warn the user and don't allow the insert
      }
   }
   
   
}





