package slupek;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class ResourceManager {

    public static void save(Serializable data, String fileName) throws Exception {
        try (ObjectOutputStream oos = new ObjectOutputStream(Files.newOutputStream(Paths.get(fileName)))){//no need close() metod call
            oos.writeObject(data);
        }
    }
    
    
    
    public static void save(BufferedImage buffImage, String extension , String fileName ){
        //Dimension size = image.getSize();
        //BufferedImage buffImage = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
        //Graphics2D g2 = buffImage.createGraphics();
        //image.paint(g2);
        try {
            ImageIO.write(buffImage, extension, new File(fileName));
            //OutputStream os = new OutputStream(Files.newOutputStream(Paths.get(fileName))) {}
            //ImageIO.write(image, fileName, os);
            //ImageIO.w
        } catch (IOException ex) {
            Logger.getLogger(ResourceManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    /*public static void save(Serializable data, String fileName) throws Exception {
        try (ObjectOutputStream oos = new ObjectOutputStream(Files.newOutputStream(Paths.get(fileName)))){//no need close() metod call
            oos.writeObject(data);
        }
    }*/
    
    

    public static Object load(String fileName) throws Exception {
        try (ObjectInputStream ois = new ObjectInputStream(Files.newInputStream(Paths.get(fileName)))) {
            return ois.readObject();
        }
    }
}
