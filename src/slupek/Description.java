package slupek;

//import java.awt.BasicStroke;
import storage.StorageProperties;
import storage.StorageData;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
//import java.awt.Stroke;
import java.awt.image.BufferedImage;

class Description {

    private static final double SCALE = 200;
    private static final int LEFT_MARGIN = 30;
    private static final int UP_MARGIN = 20;
    private static final int DOWN_MARGIN = 20;
    private static final int HEIGHT_OF_TEXT = 10;
    private static final int SPACE_LINE = 5;
   // private static final Stroke TEXT_STROKE = new BasicStroke(4f);
    //private static final Stroke BASIC_STROKE = new BasicStroke();
    private static final String FIRST_LINE = "Oznaczenie stanowiska słupowego: ";
    private static final String SECOND_LINE = "Dane wektorów:";
    private static final String RESULT = "Wynik:";
    public double textScale;
    int firstPageHeight;
    private int numberOfLines;

    String[] line;

    StorageData data;//nie prywatne
    StorageProperties dataProp;
    int width;
    int height;

    Graphics2D g2d;
    BufferedImage[] buff;

    public Description(int numberOfLines) {
        this.numberOfLines = numberOfLines;
        line = new String[numberOfLines];
        buff = new BufferedImage[2];
    }

    //createImgDescription
    void createImgDescription(Size paperSize, int beginPoint) {
        width = paperSize.getWidth();
        height = paperSize.getHeight();
        //firstPageHeight = (int)data.getScaleHigherFy();
        firstPageHeight = height - beginPoint;
        textScale = (height - width) / SCALE;
        if (textScale < 1) {
            textScale = 1;
        }

        buff[0] = null;//zerowanie tablicy - zrobic to lepiej!!!!!!!!!!!!!!
        buff[1] = null;
        createBuff(0, width, firstPageHeight);
        nextPage();
    }

    private void createBuff(int i, int w, int h) {
        buff[i] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        g2d = buff[i].createGraphics();
        /////////////////////////
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);//co to jest i do czego
        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        ////////////////////////
        g2d.setBackground(Color.WHITE);//ustawiamy tøo
        g2d.clearRect(0, 0, w, h);// malujemy na bialo za pomocá wstawienia biaøego prostokátaa
        g2d.setColor(Color.BLACK);
        g2d.setFont(new Font("Serif", java.awt.Font.ROMAN_BASELINE, HEIGHT_OF_TEXT * (int) textScale));
    }

    private void nextPage() {
        
        double nextPage = (firstPageHeight - (UP_MARGIN + DOWN_MARGIN) * textScale) / ((SPACE_LINE + HEIGHT_OF_TEXT) * textScale);
        int counter = numberOfLines + 6;

        //System.out.println(height + " heiipaperSize.getWidth() " + width + "gggg" + (height - width));
        //System.out.println(textScale1 + " counter1 "+counter1);
       System.out.println(textScale + "counter" + nextPage);
        
        int j = 0;// numery linii

        drawStringLine(g2d, FIRST_LINE + dataProp.getPoleName(), -1);//oznaczenie słupa
        for (int i = 0; i < counter; i++) {
            j++;
            System.out.println("counter:  " + i);

            if (i == (int)nextPage) {// dodawanie następnej strony
                System.out.print("next page");
                //g2d.drawImage(buff[0], 0, 0, null);
                g2d.dispose();

                createBuff(1, width, height);
                j = 0;
            }

            if (i <= 0) {
                j--;
                drawStringLine(g2d, SECOND_LINE, j);
                //g2d.drawLine(0,UP_MARGIN*(int)textScale,width,UP_MARGIN*(int)textScale);
                //g2d.drawLine(width,height-width-DOWN_MARGIN*(int)textScale,0,height-width-DOWN_MARGIN*(int)textScale);
                System.out.print("  FIRST_LINE:  ");
            } else if (i <= (line.length) && data.getForce(i - 1) == 0) {
                System.out.print("  empty line");
                j--;// zapełnia wszystkie linijki , niweluje puste
                nextPage++; //aby pojawił się wynik na stronie przy pusych polach 
            } else if (i <= (line.length)) {
                if (dataProp.isSimpleDescription()) {
                    drawStringLine(g2d, addLine(i - 1), j);          
                } else {
                    drawStringLine(g2d, addLine_Own(i - 1), j);
                }
                System.out.print("  addLine F" + i);
            } else if (i <= (line.length + 1)) {
                drawStringLine(g2d, addWind(), j);
                System.out.print("  addWind");
            } else if (i <= (line.length + 2)) {
                drawStringLine(g2d, RESULT, j);
                System.out.print("  RESULT");
            } else if (i <= (line.length + 3)) {
                drawStringLine(g2d, addResult_1(), j);
                System.out.print("  addResult_2");
            } else if (i <= (line.length + 4)) {
                drawStringLine(g2d, addResult_2(), j);
                System.out.print("  addResult_2");
            } else if (i <= (line.length + 5)) {
                drawStringLine(g2d, addFinalResult(), j);
                System.out.print("  addFinalResult");
            } else {
                System.out.println("end of loop");

                break;
            }
        }

    }

    private String addLine(int i) {

        String s1 = "F" + (i + 1) + ": siła = " + data.getForce(i) + " ["+dataProp.getUnits()+"], kąt = "
                + data.getAngle(i) + " [°] - " + "Typ linii: " + data.getLine(i)
                + " o przekroju: " + data.getProfile(i);
        String s2 = " z oblodzeniem";

        //////////////////////////////////
        if (dataProp.isIcing()) {
            return s1 + s2;
        } else {
            return s1;
        }
    }

    private String addLine_Own(int i) {

        String s1 = "F" + (i + 1) + ": siła = " + data.getForce(i) + " ["+dataProp.getUnits()+"], kąt = "
                + data.getAngle(i) + " [°] - " + data.getOwnDestription(i);
        String s2 = " z oblodzeniem";

        //////////////////////////////////
        if (dataProp.isIcing()) {
            return s1 + s2;
        } else {
            return s1;
        }
    }

    private String addWind() {

        String s = "Fwin: "+": siła = "+dataProp.getWindForce()+ " ["+dataProp.getUnits()+"], pod kątem = " 
                + dataProp.getWindAngle() + " [°] -"+" Parcie wiatru na słup i osprzęt";
        return s;
    }

    private String addResult_1() {

        String s = "Fw: Siła wypadkowa = " + data.getForceWyp() + " ["+dataProp.getUnits()+"], kąt = "
                + data.getAngleWyp() + " [°]";
        return s;
    }

    private String addResult_2() {

        if (dataProp.isAcceptFxFy()) {
            return "Dopuszczalne siły wynoszą, Fx: " + dataProp.getAcceptFx()+ " ["+dataProp.getUnits()+"]"
                    + ", Fy: " + dataProp.getAcceptFy()+ " ["+dataProp.getUnits()+"]";
        } else {
            return "Dopuszczalna siła Fdop wynosi: " + dataProp.getAcceptFw()+ " ["+dataProp.getUnits()+"]";
        }
    }

    private String addFinalResult() {

        if (dataProp.isObjectStrong()) {
            return "Stanowisko słupowe dobrano poprawnie";
        } else {
            return "Stanowisko słupowe dobrano niepoprawnie";
        }
    }

    private void drawStringLine(Graphics2D g, String str, int j) {
        g.drawString(str, LEFT_MARGIN * (int) textScale,
                (UP_MARGIN + HEIGHT_OF_TEXT + (SPACE_LINE + HEIGHT_OF_TEXT) * j) * (int) textScale);
    }

    public BufferedImage getBufferedImage(int i) {
        return buff[i];
    }
    
    public int getFirstPageHigh() {
        return firstPageHeight;
    }
}
