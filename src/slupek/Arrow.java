package slupek;

import java.awt.geom.Path2D;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class Arrow extends Path2D.Double {

    double c = 30;
    double angle = 30;

    public Arrow(double a, double b) {
        moveTo(0, 0);
        lineTo(-a * cos(Math.toRadians(angle)) / c, a * sin(Math.toRadians(angle)) / c);
        moveTo(0, 0);
        lineTo(-a * cos(Math.toRadians(angle)) / c, -a * sin(Math.toRadians(angle)) / c);
        moveTo(0, 0);
        lineTo(b, 0);
    }
}
