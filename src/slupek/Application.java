package slupek;

import storage.StorageProperties;
import storage.StorageData;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class Application {
    
    private int numberOfLines;
    
    
  JFrame mainFrame;
  
  //JPanel contentPane = new JPanel(new FlowLayout(FlowLayout.LEFT));  

    UserSpace userSpace;
    ForceGraph forceGraph;
    //ZoomGraph zoomGraph = new ZoomGraph();
    Control control;// łączymy dwa panele
    Logic logic;
    Load openFrame;
    Save saveFrame;
    StorageData data;
    StorageProperties dataProp;
    
    CreatePage createPage;
    PropertiesWindow propertiesWindow;
    //ControlProperties controlProperties = new ControlProperties();
    //ControlNew controlNew = new ControlNew();
  
  
  Application(){
      System.out.print(System.getProperty("java.specification.version"));
      startApp();
      
  }
  
    private void chooseNumberOfLines() {
        URL iconURL = getClass().getResource("/graphics/logo_czarne.png");//ikona
        ImageIcon image = new ImageIcon(iconURL);//ikona

        Object[] possibleValues = {5, 10, 15, 20, 25};
        Object selectedValue = JOptionPane.showInputDialog(
                null,
                "Choose number of lines",
                "Number of lines",
                JOptionPane.PLAIN_MESSAGE,
                image,//(icon) można wstawić ikone programu!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                possibleValues,
                possibleValues[0]);

        if ((selectedValue != null)) {
            numberOfLines = (int) selectedValue;
            //numberOfLines = Integer.parseInt(selectedValue);
        } else if (selectedValue == null) {
            //mainFrame.dispose();
            System.exit(0);
        }
    }
    
    private void startApp() {
        mainFrame = new JFrame();
        URL iconURL = getClass().getResource("/graphics/logo_1.png");
        // iconURL is null when not found
        ImageIcon icon = new ImageIcon(iconURL);
        mainFrame.setIconImage(icon.getImage());
        

        chooseNumberOfLines();
        
        userSpace = new UserSpace(numberOfLines);
        forceGraph = new ForceGraph(numberOfLines);
        control = new Control();// łączymy dwa panele
        logic = new Logic(numberOfLines);
        openFrame = new Load();
        saveFrame = new Save(numberOfLines);
        data = new StorageData(numberOfLines);
        dataProp = new StorageProperties();
        createPage = new CreatePage(numberOfLines);
        propertiesWindow = new PropertiesWindow();

        addMenu();
        setAllClasses();
        mainFrame.add(userSpace.getJPanel(), BorderLayout.WEST);
        mainFrame.add(forceGraph, BorderLayout.CENTER);
        //contentPane.add(userSpace.getJPanel(),BorderLayout.WEST);
        //contentPane.add(forceGraph,BorderLayout.CENTER);
        //mainFrame.add(contentPane);//?????????????????????????????????????????????????//
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //mainFrame.add(userSpace,BorderLayout.WEST);
        //mainFrame.add(forceGraph,BorderLayout.CENTER); 
        // mainFrame.add(zoomGraph,BorderLayout.EAST);
        mainFrame.pack();
        mainFrame.setVisible(true);

    }
  
  
    private void addMenu(){

        JMenuBar menuBar = new JMenuBar();
        mainFrame.setJMenuBar(menuBar);
        
        
         // Pierwsza kolumna menu rozwijanego
        JMenu file = new JMenu("Plik");
        menuBar.add(file);
   
        ////////////////////////////////////////////
        JMenuItem newMenuItem = file.add("Nowy");// metoda add dodaje do infomenu następny element
        newMenuItem.addActionListener((ActionEvent e)->{ //klasa anonimowa
            mainFrame.dispose();//wyłącza stary program
            startApp();          
        });
        
        JMenuItem wczytaj = file.add("Wczytaj");// metoda add dodaje do infomenu następny element
        wczytaj.addActionListener(openFrame);
        openFrame.setControl(control);
        openFrame.setStorageData(data);
        openFrame.setStorageProperties(dataProp);
        
        JMenuItem zapisz = file.add("Zapisz & Eksportuj");// metoda add dodaje do infomenu następny element
        zapisz.addActionListener(saveFrame);
        saveFrame.setCreatePage(createPage);
        saveFrame.setStorageData(data);
        saveFrame.setStorageProperties(dataProp);
        
        JMenuItem opcje = file.add("Podgląd druku");// metoda add dodaje do infomenu następny element
        opcje.addActionListener((ActionEvent e) -> {//klasa anonimowa
            JFrame printFrame = new JFrame();// bez tego nie odświeża się podgląd druku 
            printFrame.setTitle("Podgląd druku");

            createPage.createImgPages(Size.A700);
            JPanel panel_1 = new ImageToJPanel(createPage.getPage(0));
            printFrame.add(panel_1, BorderLayout.WEST);

            if (createPage.getPage(1) != null) {
                JPanel panel_2 = new ImageToJPanel(createPage.getPage(1));
                printFrame.add(panel_2, BorderLayout.CENTER);
            }
            printFrame.pack();
            printFrame.setVisible(true);

        });
        
        
       ////////////////////// Druga kolumna menu rozwijanego
        JMenu settings = new JMenu("Ustawienia");
        menuBar.add(settings);
        ///////////////////////////////////////////////////////
        
        JMenuItem vectorProperties = settings.add("Właściwości Sił");// metoda add dodaje do infomenu następny element
        vectorProperties.addActionListener((ActionEvent e)->{ //klasa anonimowa 
          propertiesWindow.setVisible();
          propertiesWindow.refresh();
          
        });
    }
    
    
    
   private void setAllClasses(){
        //setResizable(false);//pobiera wartość logiczną określającą, czy użytkownik może zmieniać rozmiar ramki.
        /////////////
        createPage.setForceGraph(forceGraph);
        createPage.setStorageData(data);
        createPage.setStorageProperties(dataProp);
        userSpace.setControl(control);// do obsługi przycisku oblicz
        forceGraph.setControl(control);//???????????????????? do odßwiezania wektorów
        control.setUserSpace(userSpace);
        control.setGraph(forceGraph);
        control.setLogic(logic);
        control.setPropertiesWindow(propertiesWindow);
        userSpace.setStorageData(data);
        propertiesWindow.setStorageProperties(dataProp);
        propertiesWindow.setStorageData(data);
        propertiesWindow.setControl(control);
        
        forceGraph.setStorageData(data);
        forceGraph.setStorageProperties(dataProp);
        ////////////////
        
        logic.setStorageData(data);
        logic.setStorageProperties(dataProp);
        userSpace.setStorageProperties(dataProp);
        //zoomGraph.setForceGraph(forceGraph);
        ///////////
    }
   
}