package slupek;

import storage.StorageProperties;
import storage.StorageData;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.net.URL;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.PlainDocument;


public class PropertiesWindow  {
   
    URL iconURL = getClass().getResource("/graphics/logo_1.png");
    // iconURL is null when not found
    ImageIcon icon = new ImageIcon(iconURL);

    static final int FIELD_LENGTH = 10;
    boolean maxWindForce = false;
    boolean icing = false;
    boolean acceptFxFy = false;
    boolean simpleDescription = false;
    
    JFrame properties = new JFrame();
    JPanel propertiesPanel = new JPanel();
    
    JPanel firstLine = new JPanel(new FlowLayout(FlowLayout.LEFT));//do lewej strony
    JPanel secondLine = new JPanel(new FlowLayout(FlowLayout.LEFT));//do lewej strony
    JPanel thirdLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel fourthLine;
    JPanel fifthLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel lastLine = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    
    
    IntFilter intFilter = new IntFilter();
    //private ControlProperties controlProperties;
    private StorageProperties dataProp;    
    private StorageData data;
    private Control control;
    
    JTextField poleNameField;
    JTextField windForceField;
    JTextField windAngleField;
    JTextField acceptFwField;
    JTextField acceptFxField;
    JTextField acceptFyField;
    JTextField unitsField;
    
    JCheckBox maxWindForceCheckBox;
    JCheckBox forceFxFyDop;
    JCheckBox icingOfWires;
    JCheckBox ownDescriptionCheckBox;
    
    String poleName;
    String units;
    double windForce;
    double windAngle;
    double acceptFw;
    double acceptFx;
    double acceptFy;
    

   public PropertiesWindow(){
       
        properties.setIconImage(icon.getImage());
        
        acceptFxField = new JTextField(FIELD_LENGTH);
        acceptFyField = new JTextField(FIELD_LENGTH);

       addFirstLine();
       addSecondLine();
       addThirdLine();
       addFourthLine();
       addFifthLine();
       addLastLine();
       
       

       propertiesPanel.setLayout(new BoxLayout(propertiesPanel, BoxLayout.Y_AXIS));
       addAll();
       
       properties.setTitle("Właściwości Sił"); 
       properties.add(propertiesPanel);
       properties.pack();
   }
   
   void setVisible(){
        
       
       properties.setVisible(true);
   }
   
   //zrobić może jakąś klasę do tego odddzielną
   private void addIntFilter(JTextField textField, IntFilter filter){
       PlainDocument doc = (PlainDocument) textField.getDocument();
      doc.setDocumentFilter(filter);
   }
   
   private void addFirstLine(){
       
       JLabel poleNameText = new JLabel("Oznaczenie słupa:");
        firstLine.add(poleNameText);

        poleNameField = new JTextField(FIELD_LENGTH);
        firstLine.add(poleNameField);
       
        ownDescriptionCheckBox = new JCheckBox("Prosty opis");
        firstLine.add(ownDescriptionCheckBox);
        ownDescriptionCheckBox.addActionListener((ActionEvent e) -> { //klasa anonimowa
            simpleDescription = ownDescriptionCheckBox.isSelected();
            if(dataProp!=null){
               dataProp.setSimpleDescription(simpleDescription);
               control.updateUserSpace();
            }
            
        });
       
   }
   
   
   private void addSecondLine() {
        JLabel windForceText = new JLabel("Siła wiatru:");
        secondLine.add(windForceText);

        windForceField = new JTextField(FIELD_LENGTH);
        secondLine.add(windForceField);
        addIntFilter(windForceField, intFilter);

        JLabel windAngleText = new JLabel("Kąt:");
        secondLine.add(windAngleText);

        windAngleField = new JTextField(FIELD_LENGTH);
        secondLine.add(windAngleField);
        addIntFilter(windAngleField, intFilter);

        maxWindForceCheckBox = new JCheckBox("Najbardziej niekorzystnie warunki");
        secondLine.add(maxWindForceCheckBox);
        maxWindForceCheckBox.addActionListener((ActionEvent e) -> { //klasa anonimowa
            maxWindForce = maxWindForceCheckBox.isSelected();
            windAngleField.setEditable(!maxWindForce);
            refresh();
            if(dataProp!=null){
               dataProp.setMaxWindForce(maxWindForce);
            }
        });
    }
   
   
   
    private void addThirdLine() {

        forceFxFyDop = new JCheckBox("Dopuszczalna siła działająca na słup w płaszczyźnie f(x,y),");
        thirdLine.add(forceFxFyDop);
        forceFxFyDop.addActionListener((ActionEvent e) -> { //klasa anonimowa
            acceptFxFy = forceFxFyDop.isSelected();
            if(dataProp!=null){
               dataProp.setAcceptFxFy(acceptFxFy);
            }
            propertiesPanel.removeAll();

            addFourthLine();
            addAll();
            //repaint();
            propertiesPanel.revalidate();
        });

        icingOfWires = new JCheckBox("Oblodzenie przewodów");
        thirdLine.add(icingOfWires);
        icingOfWires.addActionListener((ActionEvent e) -> { //klasa anonimowa
            icing = icingOfWires.isSelected();
            if(dataProp!=null){
               dataProp.setIcing(icing);
            }
        });

    }
   
   
   
    
   
   private void addFourthLine(){
       //System.out.println("AxsisXY " + acceptFxFy);
      fourthLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
      JLabel acceptForceText = new JLabel("Dopuszczalna siła działająca na słup:");
      fourthLine.add(acceptForceText);  
      
       if(acceptFxFy){
      JLabel FxText = new JLabel("Fx");
      fourthLine.add(FxText);
      
      fourthLine.add(acceptFxField);
      addIntFilter(acceptFxField,intFilter);
      
      JLabel FyText = new JLabel("Fy");
      fourthLine.add(FyText);
      
      fourthLine.add(acceptFyField);
      addIntFilter(acceptFyField,intFilter);
      
      }else{
      acceptFwField = new JTextField(FIELD_LENGTH);
      fourthLine.add(acceptFwField);
      addIntFilter(acceptFwField,intFilter);
      }
       
   }
   
   private void addFifthLine(){
       JLabel unitsText = new JLabel("Jednostka");
      fifthLine.add(unitsText);
      unitsField = new JTextField(FIELD_LENGTH);
      fifthLine.add(unitsField);
       
   }
   
   private void addLastLine(){
       JButton saveAndExit = new JButton("Save & Exit");
       lastLine.add(saveAndExit);
       saveAndExit.addActionListener((ActionEvent e) -> {//klasa anonimowa
         
        poleName = poleNameField.getText();//covert text to string
        units = unitsField.getText();
        windForce = convertTextToNumber(windForceField);
        windAngle = convertTextToNumber(windAngleField); 
        if(acceptFxFy){
        acceptFx = convertTextToNumber(acceptFxField);
        acceptFy = convertTextToNumber(acceptFyField); 
        acceptFw = 0;
        }else{
        acceptFw = convertTextToNumber(acceptFwField);
        acceptFx = 0;
        acceptFy = 0;
        }
        if(dataProp!=null){
          dataProp.setPoleName(poleName); 
          dataProp.setUnits(units); 
          dataProp.setWindForce(windForce);
          dataProp.setWindAngle(windAngle);
          dataProp.setAcceptFw(acceptFw);
          dataProp.setAcceptFx(acceptFx);
          dataProp.setAcceptFy(acceptFy);
        }
        System.out.println("windForce " + windForce);  
        System.out.println("windAngle " + windAngle);
        
        properties.dispose();// zamyka okienko
        }); 
   }
   
   
    private double convertTextToNumber(JTextField text) {
        double number;
        if (text.getText().isEmpty()) {// sprawdzanie czy string jest pusty
            number = 0;
        } else {
            number = Double.parseDouble(text.getText());
        }
        return number;
    }
    
   
   
   
   final void addAll(){// dlaczego final
     
     propertiesPanel.add(firstLine);
     propertiesPanel.add(secondLine);
     propertiesPanel.add(thirdLine);
     propertiesPanel.add(fourthLine);
     propertiesPanel.add(fifthLine);
     propertiesPanel.add(lastLine);
    }
   
    void refresh() {
       if(maxWindForce && data!=null){
       windAngleField.setText(String.valueOf(data.getAngleWyp()));
       }
    }
    
    void refreshValues(){
       
        acceptFwField.setText(String.valueOf(dataProp.getAcceptFw()));
        acceptFxField.setText(String.valueOf(dataProp.getAcceptFx()));
        acceptFyField.setText(String.valueOf(dataProp.getAcceptFy()));
        windForceField.setText(String.valueOf(dataProp.getWindForce()));
        windAngleField.setText(String.valueOf(dataProp.getWindAngle()));
        poleNameField.setText(dataProp.getPoleName());
        unitsField.setText(dataProp.getUnits());
        forceFxFyDop.setSelected(dataProp.isAcceptFxFy());
        maxWindForceCheckBox.setSelected(dataProp.isMaxWindForce());
        icingOfWires.setSelected(dataProp.isIcing());
        ownDescriptionCheckBox.setSelected(dataProp.isSimpleDescription());
    }

   
   public void  setStorageProperties(StorageProperties dataProp){
       this.dataProp = dataProp;
   }
   
   public void  setStorageData(StorageData data){
       this.data = data;
   }
   
   public void  setControl(Control control){
       this.control = control;
   }

  
 
}
