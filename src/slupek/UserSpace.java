package slupek;

import storage.StorageProperties;
import storage.StorageData;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.PlainDocument;

public class UserSpace {
private JLabel copyrightL = new JLabel("Copyright " + "\u00a9"+": Karol Poliwka 2018");
    int numberOfLines;

    final static int WIDTH_TEXT_FIELD = 5;

    //final static int LINE_AMOUNT =5;
    IntFilter intFilter = new IntFilter();
    GridBagConstraints c = new GridBagConstraints();
    JLabel lpText;
    JLabel[] lp;
    JLabel forceText;
    JLabel angleText;
    JLabel acceptText;
    JLabel lineText;
    JLabel profileLineText;
    JLabel ownDestriptionText;
    JLabel forceWypText;
    JLabel angleWypText;

    JButton calculateButton;

    JTextField forceWypField;
    JTextField angleWypField;
    JTextField[] forceField;
    JTextField[] angleField;
    double[] force;
    double[] angle;

    JTextField[] lineField;
    JTextField[] profileField;
    JTextField[] ownDestriptionField;
    String[] line;
    String[] profile;
    String[] ownDestription;

    //JTextField acceptFwField;
    // JTextField acceptFxField;
    // JTextField acceptFyField;
    //double forceWyp;
    //double angleWyp;
    boolean acceptFxFy;
    boolean strenght;
    JPanel panel = new JPanel();

    private Control control;
    private StorageData data;
    private StorageProperties dataProp;

    public UserSpace(int numberOfLines) {
        this.numberOfLines = numberOfLines;

        panel.setLayout(new GridBagLayout());
        c.fill = GridBagConstraints.HORIZONTAL; // ustawia obiekty w komórkach
        //c.fill = GridBagConstraints.CENTER; // ustawia obiekty w komórkach
        //c.ipadx = 0;
        c.weightx = 0.5;
        //c.gridheight = 1;
        makeAll();
        addAll();

    }

    final void makeAll() {
        makeLpColumn();
        makeForceColumn();
        makeAngleColumn();
        makeLineColumn();
        makeProfileColumn();
        makeOwnDescription();
        makeEnd();
    }

    final void addAll() {
        
        addLpColumn();
        addForceColumn();
        addAngleColumn();

        if (dataProp != null && dataProp.isSimpleDescription()) {
            addLineColumn();
            addProfileColumn();
        } else {
            addOwnDescription();
            
        }
        addEnd();
        addCopyright();
    }

    public void repaintGraphics() {
        panel.removeAll();
        addAll();
        panel.repaint();
        panel.revalidate();
    }

    private void makeLpColumn() {
        
        lpText = new JLabel("Lp.");
        lp = new JLabel[numberOfLines];
        for (int i = 0; i < lp.length; i++) {
            lp[i] = new JLabel("F" + (i + 1));
        }
    }

     
    private void addLpColumn() {
        c.insets = new Insets(0, 20, 0, 0);
        c.gridx = 0;
        c.gridy = 1;
        panel.add(lpText, c);
        for (int i = 0; i < lp.length; i++) {
            c.gridx = 0;
            c.gridy = i + 2;
            panel.add(lp[i], c);
        }
        //panel.add(copyrightL, c);
    }

    private void makeForceColumn() {
        forceText = new JLabel("Siła");
        forceField = new JTextField[numberOfLines];
        for (int i = 0; i < forceField.length; i++) {
            forceField[i] = new JTextField(WIDTH_TEXT_FIELD);
        }
    }

    private void addForceColumn() {
        c.insets = new Insets(0, 10, 0, 0);
        c.gridx = 1;
        c.gridy = 1;
        panel.add(forceText, c);
        for (int i = 0; i < forceField.length; i++) {
            c.gridx = 1;
            c.gridy = i + 2;
            panel.add(forceField[i], c);
            addIntFilter(forceField[i], intFilter);
        }
    }

    private void makeAngleColumn() {
        angleText = new JLabel("Kąt");
        angleField = new JTextField[numberOfLines];
        for (int i = 0; i < angleField.length; i++) {
            angleField[i] = new JTextField(WIDTH_TEXT_FIELD);
        }
    }

    private void addAngleColumn() {
        c.gridx = 2;
        c.gridy = 1;
        panel.add(angleText, c);
        for (int i = 0; i < angleField.length; i++) {
            c.gridx = 2;
            c.gridy = i + 2;
            panel.add(angleField[i], c);
            addIntFilter(angleField[i], intFilter);
        }
    }

    private void makeLineColumn() {
        lineText = new JLabel("Typ linii");
        lineField = new JTextField[numberOfLines];
        for (int i = 0; i < numberOfLines; i++) {
            lineField[i] = new JTextField(2*WIDTH_TEXT_FIELD);
        }
    }

    private void addLineColumn() {
        c.gridx = 3;
        c.gridy = 1;
        panel.add(lineText, c);
        for (int i = 0; i < numberOfLines; i++) {
            c.gridx = 3;
            c.gridy = i + 2;
            panel.add(lineField[i], c);
        }
    }

    private void makeProfileColumn() {
        profileLineText = new JLabel("Przekrój");
        profileField = new JTextField[numberOfLines];
        for (int i = 0; i < numberOfLines; i++) {
            profileField[i] = new JTextField(2*WIDTH_TEXT_FIELD);
        }
    }

    private void addProfileColumn() {
        c.gridx = 4;
        c.gridy = 1;
        panel.add(profileLineText, c);
        for (int i = 0; i < numberOfLines; i++) {
            c.gridx = 4;
            c.gridy = i + 2;
            panel.add(profileField[i], c);
        }
    }

    private void makeOwnDescription() {
        ownDestriptionText = new JLabel("Własny opis");
        ownDestriptionField = new JTextField[numberOfLines];
        for (int i = 0; i < numberOfLines; i++) {
            ownDestriptionField[i] = new JTextField(6*WIDTH_TEXT_FIELD);
        }
    }

    private void addOwnDescription() {
        c.gridx = 3;
        c.gridwidth = 2;
        c.gridy = 1;
        panel.add(ownDestriptionText, c);
        for (int i = 0; i < numberOfLines; i++) {
            c.gridx = 3;
            c.gridy = i + 2;
            panel.add(ownDestriptionField[i], c);
        }
        c.gridwidth = 1;
    }

    private void makeEnd() {
        calculateButton = new JButton("Oblicz");
        calculateButton.addActionListener((ActionEvent e) -> {//klasa anonimowa
            if (control != null && data != null) {
                convertTextToNumber();
                control.calculateAction();
                control.repaintGraphAction();// poprzez klase control odświezamy forcegraph

                //control.updateWestPanel();
                angleWypField.setText(String.valueOf(data.getAngleWyp()));
                forceWypField.setText(String.valueOf(data.getForceWyp()));

                System.out.println(dataProp.isObjectStrong() + "objectStrong");
                convertTextToString();
                checkStrong();
            }
        });

        acceptText = new JLabel("empty");
        acceptText.setEnabled(false);
        forceWypText = new JLabel("Fw:");
        forceWypField = new JTextField(WIDTH_TEXT_FIELD);
        forceWypField.setEditable(false);
        angleWypText = new JLabel("Kąt wypadkowy:");
        angleWypField = new JTextField(WIDTH_TEXT_FIELD);
        angleWypField.setEditable(false);
    }

    private void addEnd() {
        c.gridx = 1;
        c.gridwidth = 2;
        c.gridy = numberOfLines + 2;
        panel.add(calculateButton, c);

        c.gridx = 3;
        c.gridwidth = 2;
        c.gridy = numberOfLines + 2;
        panel.add(acceptText, c);

        c.gridx = 1;
        c.gridwidth = 1;
        c.gridy = numberOfLines + 3;
        panel.add(forceWypText, c);

        c.gridx = 2;
        c.gridwidth = 1;
        c.gridy = numberOfLines + 3;
        panel.add(forceWypField, c);

        c.gridx = 3;
        c.gridwidth = 1;
        c.gridy = numberOfLines + 3;
        panel.add(angleWypText, c);

        c.gridx = 4;
        c.gridwidth = 1;
        c.gridy = numberOfLines + 3;
        panel.add(angleWypField, c);
    }
    
    private void addCopyright() {
        copyrightL.setForeground(Color.GRAY);
        copyrightL.setFont(new Font("Serif", Font.PLAIN, 10));
        c.insets = new Insets(0, 20, 0, 0);
        c.gridx = 0;
        c.gridy = numberOfLines + 4;
        c.gridwidth = 5;

        panel.add(copyrightL, c);
        c.gridwidth = 1;
    }

    /*private void makeAcceptText(){
        JLabel acceptText = new JLabel();
        if(objectStrong){
            System.out.println(objectStrong+"Słup wytrzyma");
            
            acceptText.setText("Słup wytrzyma");
            acceptText.repaint();
           
        }else{
            System.out.println(objectStrong+"\"Słup nie wytrzyma");
           acceptText.setText("Słup nie wytrzyma");
           acceptText.repaint();
        }
        
        
        acceptText.setEnabled(false);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 3;
        c.gridwidth = 1;
        c.gridy = numberOfLines + 1;
        
        
        add(acceptText, c);
    }*/

    private void addIntFilter(JTextField textField, IntFilter filter) {
        PlainDocument doc = (PlainDocument) textField.getDocument();
        doc.setDocumentFilter(filter);
    }

    private void checkStrong() {
        if (dataProp != null && dataProp.isObjectStrong()) {

            acceptText.setText("Słup wytrzyma");

        } else if (dataProp != null) {

            acceptText.setText("Słup nie wytrzyma");
        }
    }

    private void convertTextToNumber() {
        System.out.print("  getTextToNumber  ");
        force = new double[numberOfLines];
        angle = new double[numberOfLines];

        for (int i = 0; i < numberOfLines; i++) {
            if (forceField[i].getText().isEmpty()) {// sprawdzanie czy string jest pusty
                force[i] = 0;
            } else {
                force[i] = Double.parseDouble(forceField[i].getText());
            }

            if (angleField[i].getText().isEmpty()) {
                angle[i] = 0;
            } else {
                angle[i] = Double.parseDouble(angleField[i].getText());
            }
        }
        data.setForce(force);
        data.setAngle(angle);
    }

    public void refreshValuesOnJTextField() {// funkcja słuząca klasy LOAD
        for (int i = 0; i < numberOfLines; i++) {
          //  if (data.getForce(i) != 0) {
                forceField[i].setText(String.valueOf(data.getForce(i)));
           // } else {
          // forceField[i] = null;
          // }
          // if (data.getAngle(i) != 0) {
                angleField[i].setText(String.valueOf(data.getAngle(i)));
            //} else {
           //     angleField[i] = null;
           // }
            lineField[i].setText(data.getLine(i));
            profileField[i].setText(data.getProfile(i));
            ownDestriptionField[i].setText(data.getOwnDestription(i));
            
            checkStrong();
        }
    }

    void convertTextToString() {
        line = new String[numberOfLines];
        profile = new String[numberOfLines];
        ownDestription = new String[numberOfLines];
        for (int i = 0; i < numberOfLines; i++) {
            line[i] = lineField[i].getText();
            profile[i] = profileField[i].getText();
            ownDestription[i] = ownDestriptionField[i].getText();
        }
        data.setLine(line);
        data.setProfile(profile);
        data.setOwnDestription(ownDestription);
    }

    public void setControl(Control control) {// funkcje ustawiania wartości, można by odwołać się do samej wartości
        this.control = control;
    }

    public void setStorageData(StorageData data) {
        this.data = data;
    }

    public void setStorageProperties(StorageProperties dataProp) {
        this.dataProp = dataProp;
    }

    public JPanel getJPanel() {
        return panel;
    }

    public void setAngleWypField(double angleWyp) {// funkcje ustawiania wartości, można by odwołać się do samej wartości
        angleWypField.setText(String.valueOf(angleWyp));

    }

    public void setForceWypField(double forceWyp) {// funkcje ustawiania wartości, można by odwołać się do samej wartości
        forceWypField.setText(String.valueOf(forceWyp));
    }


    /*
    public void setAngleWyp(double angleWyp) {// funkcje ustawiania wartości, można by odwołać się do samej wartości
        this.angleWyp = angleWyp;
    }

    public void setForceWyp(double forceWyp) {// funkcje ustawiania wartości, można by odwołać się do samej wartości
        this.forceWyp = forceWyp;
    }*/
 /*public void setNumberOfLines(int numberOfLines){
        this.numberOfLines = numberOfLines;
    }*/
 /* public void setAcceptFwField(double acceptFw){
        //String s = 
        acceptFwField.setText(String.valueOf(acceptFw)); 
    }*/
}
