package slupek;

import storage.StorageProperties;
import storage.StorageData;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;




public class CreatePage  {
    
    
    
    private StorageData data;
    private  ForceGraph forceGraph;
    
    Dimension dim;
    Description description;
    JPanelToImage jPanelToImage = new JPanelToImage();
    
    BufferedImage[] page = new BufferedImage[2];
    
    public CreatePage(int numberOfLines){
       description = new Description(numberOfLines); 
    }
    
    
    
    public void createImgPages(Size paperSize){//tworzymy strone popzez dodanie różnych obrazów
       
       // if(forceGraph!=null)
       int w = paperSize.getWidth();
       int h = paperSize.getHeight();
        dim = forceGraph.getPreferredSize();
        forceGraph.setSize(w,w);//ustalamy wielkość wykresu (rozmiar A4 lub A8)
        // przed funkcja createImage wkładany panel powinien posiadać rozmiar
        BufferedImage buffForceGraph = jPanelToImage.createImage(forceGraph);// zmienić na klasę
        forceGraph.setSize(dim);//powrót do ustawień poczatkowych
        
        int textUP = (int)(buffForceGraph.getHeight()*data.getScaleHigherFy());
        description.createImgDescription(paperSize,textUP);
        page[1] = description.getBufferedImage(1);//czasami null wyciąga
        
        page[0] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);//poprawić to cos mi nie pasuje
        Graphics g = page[0].getGraphics();
        g.drawImage(buffForceGraph, 0, 0, null);
        g.drawImage(description.getBufferedImage(0), 0, /*description.getFirstPageHigh()*/textUP, null);
        g.dispose();
        
    }
    
    
    
    
    
    public void setForceGraph(ForceGraph forceGraph) {
   this.forceGraph = forceGraph;
   }
   
    public void  setStorageData(StorageData data){
        description.data = data;
        this.data = data;
    }
    
 
     
     public void  setStorageProperties(StorageProperties dataProp){
        description.dataProp = dataProp;
    }
     
     public BufferedImage getPage(int i){
       return page[i]; 
    }
    
    
     /*public void setIcing(boolean icing) {
   description.icing = icing;
   }*/
    
    
    /*void setDefault(){
         //dim = forceGraph.getPreferredSize();
          forceGraph.setSize(dim);
          //forceGraph.repaint();
    }*/
    
    
    //JPanel createPanelPage(Size paperSize){//tworzymy sklejone obrazy w panelu w celu podglądnięcia przed drukiem

     //   return new ImageToJPanel(createImgPage(paperSize));
   // }
    
    
    
            
    
}
