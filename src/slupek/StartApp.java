package slupek;

import java.awt.EventQueue;

/**
 *
 * @author Karol
 */
public class StartApp {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Application();
            }
        });
         
    }
}
