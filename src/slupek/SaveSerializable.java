package slupek;


class SaveSerializable implements java.io.Serializable{
 
    private static final long serialVersionUID = 1L;
    //private int numberOfLines;
    ////////////////////////////////////////////
    public double acceptFw;
    public double acceptFx;
    public double acceptFy;
    public boolean acceptFxFy;
    public boolean maxWindForce;
    public boolean icing;
    public double windForce;
    public double windAngle;
    public boolean objectStrong;
    public boolean simpleDescription;
    public String poleName;
    public String units;
    ////////////////////////////////////
    public String fileName;
    public double[] firstColumn;
    public double[] secondColumn;
    public String[] thirdColumn;
    public String[] fourthColumn;
   public String[] fifthColumn;
   
   
   
  
    public SaveSerializable(int numberOfLines) {
        //this.numberOfLines = numberOfLines;
        firstColumn = new double[numberOfLines];
        secondColumn = new double[numberOfLines];
        thirdColumn = new String[numberOfLines];
        fourthColumn = new String[numberOfLines];
        fifthColumn = new String[numberOfLines];
        
        
        
        
    }
    
    
}
