package slupek;


public class Control {
   private ForceGraph graph;
   private UserSpace userSpace;
   private Logic logic;
   private PropertiesWindow propertiesWindow;
   
   
   
  public void repaintGraphAction() {
      if (graph != null) {// jakieś zabezpieczenie przed zerem - zastosować wszędzie!!!
         graph.repaint();
         System.out.println("repainr");
      }
   }
   
  public void calculateAction() {
      
      if (logic != null) { // jakieś zabezpieczenie przed zerem - zastosować wszędzie!!!
         logic.forces();
         System.out.println("control logic");
      }
   }
  
   public void loadData(){
       
       userSpace.refreshValuesOnJTextField();
       System.out.println("load data");
   }
  public void loadDataProp(){
       
       propertiesWindow.refreshValues();
       System.out.println("load data");
   }

  
  public void updateGraph(){

      graph.scaleVectors();
  }
  
  public void updateUserSpace(){
System.out.println("updateUserSpace");
      userSpace.repaintGraphics();
  }

  
   public void setGraph(ForceGraph graph) {// funkcje ustawiania wartości, można by odwołać się do samej wartości
      this.graph = graph;
   }

   public void setUserSpace(UserSpace userSpace) {
      this.userSpace = userSpace;
   }
   
   public void setLogic(Logic logic) { 
      this.logic = logic;
   }
   
   public void setPropertiesWindow(PropertiesWindow propertiesWindow){
       this.propertiesWindow = propertiesWindow;
   }
  
   
}