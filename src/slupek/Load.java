package slupek;

import storage.StorageProperties;
import storage.StorageData;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;


public class Load implements ActionListener  {
    
    JFrame load = new JFrame();
    URL iconURL = getClass().getResource("/graphics/logo_1.png");
    // iconURL is null when not found
    ImageIcon icon = new ImageIcon(iconURL);

    JFileChooser chooser;
    LookAndFeel previousLF;

    String sav = ".sav";
       
    ExtensionFileFilter savExt = new ExtensionFileFilter(sav,sav);
    private Control control;
    private StorageProperties dataProp;
    private StorageData data;
    SaveSerializable savSerial;
    
    File workingDirectory = new File(System.getProperty("user.dir"));

   public Load(){
       load.setIconImage(icon.getImage());
   }
    


    @Override
    public void actionPerformed(ActionEvent e) {
        
        
        
       previousLF = UIManager.getLookAndFeel();
       chooser = new SystemJFileChooser();
        
       
       
       chooser.setCurrentDirectory(workingDirectory);
       
       try {
            UIManager.setLookAndFeel(previousLF);// zmienia na domyślny interfejs
        } catch (UnsupportedLookAndFeelException ex) {
            //Logger.getLogger(Save.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       
       chooser.addChoosableFileFilter(savExt);
       chooser.setAcceptAllFileFilterUsed(false);// wyøácza wybór "allfiles" 
          
              
        int value = chooser.showOpenDialog(load);
      if (value == JFileChooser.APPROVE_OPTION) {
          try {
            //String path = chooser.getSelectedFile().getName();//tylko nazwa pliku
            String path = chooser.getSelectedFile().getAbsolutePath();//ścieżka cała do pliku
            //SaveData data = (StorageData) ResourceManager.load(fieldName.getText());
            savSerial = (SaveSerializable) ResourceManager.load(path);

            loadDataFromSave();
            
            
            
            
            System.out.println(path+"data.getFor load: " );
            control.loadData();
            control.loadDataProp();
            
            System.out.println("Open");

            }
            catch (Exception ex) {
                System.out.println("Couldn't load: " + ex.getMessage());
            }          
      }
  
    }
    
    private void loadDataFromSave(){
        data.setForce(savSerial.firstColumn);
        data.setAngle(savSerial.secondColumn);
        data.setLine(savSerial.thirdColumn);
        data.setProfile(savSerial.fourthColumn);
        data.setOwnDestription(savSerial.fifthColumn);

        dataProp.setAcceptFw(savSerial.acceptFw);
        dataProp.setAcceptFx(savSerial.acceptFx);
        dataProp.setAcceptFy(savSerial.acceptFy);
        dataProp.setAcceptFxFy(savSerial.acceptFxFy);
        dataProp.setMaxWindForce(savSerial.maxWindForce);
        dataProp.setIcing(savSerial.icing);
        dataProp.setWindForce(savSerial.windForce);
        dataProp.setWindAngle(savSerial.windAngle);
        dataProp.setObjectStrong(savSerial.objectStrong);
        dataProp.setSimpleDescription(savSerial.simpleDescription);
        dataProp.setPoleName(savSerial.poleName);
        dataProp.setUnits(savSerial.units);   
                
    }
    
    public void setControl(Control control) {
      this.control = control;
   }
    
    public void setStorageData(StorageData data) {
        this.data = data;
    }

    public void setStorageProperties(StorageProperties dataProp) {
        this.dataProp = dataProp;
    }
}

