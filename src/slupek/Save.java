package slupek;

import storage.StorageProperties;
import storage.StorageData;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Save implements ActionListener {
    
    JFrame save = new JFrame();
    URL iconURL = getClass().getResource("/graphics/logo_1.png");
    // iconURL is null when not found
    ImageIcon icon = new ImageIcon(iconURL);   
    
String sav = ".sav";
String jpg = ".jpg";
String png = ".png";
String bmp = ".bmp";

ExtensionFileFilter savExt = new ExtensionFileFilter(sav,sav);
ExtensionFileFilter jpgExt = new ExtensionFileFilter(jpg,jpg);
ExtensionFileFilter pngExt = new ExtensionFileFilter(png,png);
ExtensionFileFilter bmpExt = new ExtensionFileFilter(bmp,bmp);

File workingDirectory = new File(System.getProperty("user.dir"));
JFileChooser chooser;
LookAndFeel previousLF;

SaveSerializable savSerial;
String path;
File file;

private CreatePage createPage;
private StorageData data;
private StorageProperties dataProp;

JPanelToImage createImage = new JPanelToImage(); 
JPanel panel1 = new JPanel();

public Save(int numberOfLines){
       save.setIconImage(icon.getImage()); 
       savSerial = new SaveSerializable(numberOfLines); 
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        previousLF = UIManager.getLookAndFeel();
        chooser = new SystemJFileChooser();
        
        chooser.setCurrentDirectory(workingDirectory);
      
        try {
            UIManager.setLookAndFeel(previousLF);// zmienia na domyślny 
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Save.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        chooser.addChoosableFileFilter(savExt);
        chooser.addChoosableFileFilter(jpgExt);
        chooser.addChoosableFileFilter(pngExt);
        chooser.addChoosableFileFilter(bmpExt);
        
    chooser.setAcceptAllFileFilterUsed(false);  // turns of choice "all files"

            
    int value = chooser.showSaveDialog(save);   

    System.out.println(savExt+"  :savExt:  " + chooser.getFileFilter());
    System.out.println(jpgExt+"  :jpgExt:  " + chooser.getFileFilter());
    if (value == JFileChooser.APPROVE_OPTION && chooser.getFileFilter().equals(savExt))
        saveAsSavFile(sav);         
    else if (value == JFileChooser.APPROVE_OPTION && chooser.getFileFilter().equals(jpgExt))
        makeImageFile(jpg);         
    else if (value == JFileChooser.APPROVE_OPTION && chooser.getFileFilter().equals(pngExt))
        makeImageFile(png);         
    else if (value == JFileChooser.APPROVE_OPTION && chooser.getFileFilter().equals(bmpExt)) 
        makeImageFile(bmp);               
    }
    
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //put this in the commit comment, in English ofc: nie dziaøa gdy zapiszesz jedna kolumné póniej             napiszesz cos w drugiej to po wczytaniu druga sié nie wyzeruje!!!!!!!!!!!!!!!
    //mark as [WIP] (work in progress), since it doesn't work yet.
    public void saveAsSavFile(String ext){
        path = chooser.getSelectedFile().getAbsolutePath(); //read more:    https://www.coffeecup.com/help/articles/absolute-vs-relative-pathslinks/
        file = chooser.getSelectedFile();
            
        putDataToSave();       
        try {
                if(file.exists()){
                ResourceManager.save(savSerial, path);
                    System.out.println("Save:  " + path);
                } else{
                ResourceManager.save(savSerial, path + ext);
                System.out.println("Save:  " + path + ext);
                }
            }
            catch (Exception ex) {
                System.out.println("Couldn't save: " + ex.getMessage());
            }      
        }
    
    
    public void makeImageFile(String ext){
        path = chooser.getSelectedFile().getAbsolutePath();//ścieżka cała do pliku??
        file = chooser.getSelectedFile();   

        createPage.createImgPages(Size.A4);
        try {
            if(file.exists()) {
                ResourceManager.save(createPage.getPage(0), ext.substring(1), path);
                if(createPage.getPage(1)!=null)
                    ResourceManager.save(createPage.getPage(1), ext.substring(1), path+"_1");
                System.out.println("Panel saved as Image." + path);    
            } else {
                ResourceManager.save(createPage.getPage(0), ext.substring(1), path + ext);
                if(createPage.getPage(1)!=null)
                    ResourceManager.save(createPage.getPage(1), ext.substring(1), path+"_1"+ext);  
                System.out.println("Save:  " + path +  ext);
            }
        }
        catch (Exception ex) {
            System.out.println("Couldn't save: " + ex.getMessage());
        }      
    }
    
    private void putDataToSave() {        
        savSerial.firstColumn = data.getForce(); 
        savSerial.secondColumn = data.getAngle(); 
        savSerial.thirdColumn = data.getLine();
        savSerial.fourthColumn = data.getProfile();
        savSerial.fifthColumn = data.getOwnDestription();
        
        savSerial.acceptFw = dataProp.getAcceptFw();
        savSerial.acceptFx = dataProp.getAcceptFx();
        savSerial.acceptFy = dataProp.getAcceptFy();
        savSerial.acceptFxFy = dataProp.isAcceptFxFy();
        savSerial.maxWindForce = dataProp.isMaxWindForce();
        savSerial.icing = dataProp.isIcing();
        savSerial.windForce = dataProp.getWindForce();
        savSerial.windAngle = dataProp.getWindAngle();
        savSerial.objectStrong = dataProp.isObjectStrong();
        savSerial.simpleDescription = dataProp.isSimpleDescription();
        savSerial.poleName = dataProp.getPoleName();
        savSerial.units = dataProp.getUnits();
    }
    
    public void setStorageData(StorageData data) {
        this.data = data;
    }

    public void setStorageProperties(StorageProperties dataProp) {
        this.dataProp = dataProp;
    }

    public void setCreatePage(CreatePage createPage) {
        this.createPage = createPage;
    }
}
